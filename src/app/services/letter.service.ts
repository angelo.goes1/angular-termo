import { EventEmitter, Injectable } from '@angular/core';
import { Subject, delay } from 'rxjs';
import { environment } from 'src/environments/environment';
import { KeyState } from '../models/keystate';

@Injectable({
  providedIn: 'root',
})
export class LetterService {
  fullKeyboard: string[] = environment.QWERTY_KLayout.split('');
  keyboardEvent: EventEmitter<KeyState[]> = new EventEmitter<KeyState[]>();
  defaultKeyboard: KeyState[] = Array(this.fullKeyboard.length).fill(
    KeyState.clear
  );
  currentKeyboard: KeyState[] = this.defaultKeyboard;

  positionEvent: EventEmitter<number> = new EventEmitter();
  enterGuessEvent: EventEmitter<string> = new EventEmitter();

  winEvent: EventEmitter<any> = new EventEmitter();
  loseEvent: EventEmitter<any> = new EventEmitter();
  restartEvent: EventEmitter<any> = new EventEmitter();

  constructor() {}
  ngOnInit(): void {}

  updateKeyboard(word: string, right: string[], place: string[]) {
    for (let i = 0; i < word.length; i++) {
      var state = KeyState.wrong;

      if (place.includes(word[i])) state = KeyState.place;
      if (right.includes(word[i])) state = KeyState.right;

      this.currentKeyboard[this.fullKeyboard.indexOf(word[i])] = state;
    }
    this.keyboardEvent.emit(this.currentKeyboard);
  }
}
