import { MenuService } from './../../../services/menu.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-configs',
  templateUrl: './configs.component.html',
  styleUrls: ['./configs.component.scss']
})
export class ConfigsComponent implements OnInit {

  constructor(private menuService: MenuService) { }

  ngOnInit(): void {
  }

  closeMenu(event: MouseEvent) {
    console.log(event);
    
    this.menuService.openMenu.emit('');
  }

}
