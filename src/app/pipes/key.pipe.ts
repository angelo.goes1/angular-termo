import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'key',
})
export class KeyPipe implements PipeTransform {
  transform(value: string): string {
    if (value == 'Backspace') return '<';
    if (value == 'Enter') return '^';
    if (value == '^') return 'ENTER';
    if (value == '<') return '';
    return value;
  }
}
