import { EventEmitter, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AnimationService {
  jumpGuessAnimation: EventEmitter<number> = new EventEmitter();
  turnGuessAnimation: EventEmitter<number> = new EventEmitter();
  shakeGuessAnimation: EventEmitter<number> = new EventEmitter();
  notifyAnimation: EventEmitter<string> = new EventEmitter();

  constructor() {}
}
