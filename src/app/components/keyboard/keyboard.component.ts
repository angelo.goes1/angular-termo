import { Component, HostListener, OnInit, QueryList, ViewChildren } from '@angular/core';
import { KeyPipe } from 'src/app/pipes/key.pipe';
import { AnimationService } from 'src/app/services/animation.service';
import { LetterService } from 'src/app/services/letter.service';

import { KeyComponent } from './key/key.component';

@Component({
  selector: 'app-keyboard',
  templateUrl: './keyboard.component.html',
  styleUrls: ['./keyboard.component.scss'],
})
export class KeyboardComponent implements OnInit {
  fullKeyboard: string[];

  @ViewChildren(KeyComponent) keys!: QueryList<KeyComponent>;

  constructor(
    private letterService: LetterService,
    private animationService: AnimationService,
    private keyPipe: KeyPipe
  ) {
    this.fullKeyboard = letterService.fullKeyboard;

    letterService.keyboardEvent.subscribe((states) => {
      for (let i = 0; i < this.fullKeyboard.length; i++) {
        this.keys.get(i)?.setState(states[i]);
      }
    });
  }

  ngOnInit(): void {}

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) { 
    var character = this.keyPipe.transform(event.key);
    this.letterService.enterGuessEvent.emit(character);
  }
}
