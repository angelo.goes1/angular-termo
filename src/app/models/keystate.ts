export enum KeyState {
    clear = 'clear',
    right = 'right',
    wrong = 'wrong',
    place = 'place',
  }
  