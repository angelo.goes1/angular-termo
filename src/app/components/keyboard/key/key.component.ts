import { AnimationService } from 'src/app/services/animation.service';
import {
  Component,
  HostListener,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';

import { KeyState } from '../../../models/keystate';
import { LetterService } from '../../../services/letter.service';

@Component({
  selector: 'app-key',
  templateUrl: './key.component.html',
  styleUrls: ['./key.component.scss'],
})
export class KeyComponent implements OnInit {
  @Input() mykey!: string;
  state: KeyState = KeyState.clear;

  touched: boolean = false;

  constructor(
    private letterService: LetterService,
    private animationService: AnimationService
  ) {}

  ngOnInit(): void {
  }

  setState(state: KeyState){
    if(this.state == KeyState.place) this.touched = true; 
    this.state = state;
  }

  enterKey() {  
    this.letterService.enterGuessEvent.emit(this.mykey);
  }
}
