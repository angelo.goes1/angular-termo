import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { AnimationService } from 'src/app/services/animation.service';
import { environment } from 'src/environments/environment';

import palavras from '../../../assets/palavras.json';
import { LetterService } from './../../services/letter.service';
import { GuessComponent } from './guess/guess.component';

@Component({
  selector: 'app-guesses',
  templateUrl: './guesses.component.html',
  styleUrls: ['./guesses.component.scss'],
})
export class GuessesComponent implements OnInit {
  round: number = 0;
  selectedId: number = 0;
  guesses: string[] = Array(30).fill('');
  words: string[] = palavras;
  today: number = new Date().setHours(0, 0, 0, 0);
  answer: string = this.words[this.today % this.words.length];
  normalizedAnswer: string;
  animationOver: boolean = true;

  @ViewChildren(GuessComponent) children!: QueryList<GuessComponent>;

  constructor(
    private letterService: LetterService,
    private animationService: AnimationService
  ) {
    this.normalizedAnswer = this.answer
      .normalize('NFKD')
      .replace(/[\u0300-\u036f]/g, '');
    console.log('Resposta: ' + this.answer);
    console.log('Resposta normalizada: ' + this.normalizedAnswer);
  }

  ngOnInit(): void {
    this.letterService.positionEvent.subscribe((index) => {
      this.selectedId = index;
    });

    this.letterService.enterGuessEvent.subscribe((key: string) => {
      if (this.animationOver) {
        var row = this.round * 5;
        this.animationOver = false;

        switch (key) {
          case '^':
            var answer = this.answer;
            var norm = this.normalizedAnswer;
            
            var letters = '';
            for (let i = 0; i < 5; i++) {
              letters += this.children.get(row + i)?.letter;
            }

            var checking = false;
            for (let i = 0; i < this.words.length; i++) {
              var original = this.words[i];
              var normalized = original
                .normalize('NFKD')
                .replace(/[\u0300-\u036f]/g, '');
              if (letters == normalized && letters.length >= 5) {
                letters = original;
                checking = true;
                break;
              }
            }

            if (checking) {
              var rightIndexes: number[] = [];
              var placeIndexes: number[] = [];

              for (let i = 0; i < 5; i++) {
                // Update guess blocks in case of accentuation
                this.setGuessLetterById(row + i, letters[i], false);

                if (
                  letters[i] == answer[i] ||
                  letters[i] == norm[i]
                ) {
                  answer =
                    answer.substring(0, i) + '_' + answer.substring(i + 1);
                  this.setGuessStateById(row + i, 'right');
                  rightIndexes.push(row + i);
                }
              }

              console.log(answer);

              for (let i = 0; i < 5; i++) {
                if (
                  (answer.includes(letters[i]) ||
                    norm.includes(letters[i])) &&
                  letters[i] != answer[i]
                ) {
                  this.setGuessStateById(row + i, 'place');
                  placeIndexes.push(row + i);
                }
              }

              this.delayedAnimation(row, 'flip', environment.singleTurnDelayMs);

              setTimeout(() => {
                this.letterService.updateKeyboard(
                  letters,
                  this.mapGuessIndexesToLetterList(rightIndexes),
                  this.mapGuessIndexesToLetterList(placeIndexes)
                );

                if (rightIndexes.length >= 5) {
                  this.delayedAnimation(
                    row,
                    'jump',
                    (2 * environment.singleTurnDelayMs) / 8
                  );
                  var message = '';
                  switch (this.round) {
                    case 0:
                      message = 'Genial';
                      break;
                    case 1:
                      message = 'Fantástico';
                      break;
                    case 2:
                      message = 'Extraordinário';
                      break;
                    case 3:
                      message = 'Fenomenal';
                      break;
                    case 4:
                      message = 'Impresionante';
                      break;
                    default:
                      message = 'Ufa!';
                  }

                  this.animationService.notifyAnimation.emit(message);
                } else {
                  this.round++;
                  this.selectedId = this.round * 5;
                  this.animationOver = true;
                }
              }, environment.roundTurnDelayMs);
            } else {
              this.animationOver = true;

              for (let i = 0; i < 5; i++)
                this.children.get(row + i)?.guessShake();

              this.animationService.notifyAnimation.emit(
                letters.length != 5
                  ? 'só palavras com 5 letras'
                  : 'essa palavra não é aceita'
              );
            }
            break;

          case '<':
            this.animationOver = true;
            this.animationService.notifyAnimation.emit();
            if (this.selectedId > 100) this.selectedId = (this.round + 1) * 5;

            if (
              this.children.get(this.selectedId)?.letter ||
              this.moveSelection('left')
            ) {
              this.setGuessLetterById(this.selectedId, '', false);
            }
            break;

          case 'ArrowRight':
            this.moveSelection('right');
            break;

          case 'ArrowLeft':
            this.moveSelection('left');
            break;

          default:
            this.animationOver = true;
            if (this.letterService.fullKeyboard.includes(key)) {
              if (this.selectedId < 100) {
                this.setGuessLetterById(this.selectedId, key);
                this.moveSelection('right');
              }
            }
            break;
        }
        this.animationOver = true;
      }
    });

    setTimeout(() => this.letterService.positionEvent.emit(this.selectedId));
  }

  moveSelection(side: string): boolean {
    if (side == 'right') {
      if (this.selectedId + 1 < 5 * (this.round + 1)) {
        this.selectedId++;
        return true;
      } else {
        this.selectedId = 101;
        return false;
      }
    } else if (side == 'left' && this.selectedId - 1 >= 5 * this.round) {
      if (this.selectedId > 100) this.selectedId = (this.round + 1) * 5;
      this.selectedId--;
      return true;
    }
    return false;
  }

  mapGuessIndexesToLetterList(indexes: number[]): string[] {
    return indexes.map((i) => this.children.get(i)!.letter);
  }

  setGuessLetterById(
    id: number,
    letter: string,
    animate: boolean = true
  ): void {
    this.children.get(id)?.setLetter(letter, animate);
  }

  setGuessStateById(id: number, state: string) {
    this.children.get(id)?.setState(state);
  }

  delayedAnimation(row: number, animation: string, delayMs: number): void {
    console.log('will now ' + animation);

    for (let i = 0; i < 5; i++) {
      let delay = i * delayMs;
      setTimeout(() => {
        if (animation == 'flip') this.children.get(row + i)?.guessFlip();
        else this.children.get(row + i)?.guessJump();
      }, delay);
    }
  }
}
