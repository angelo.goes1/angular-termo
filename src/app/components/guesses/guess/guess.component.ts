import { TmplAstRecursiveVisitor } from '@angular/compiler';
import { Component, Input, OnInit } from '@angular/core';
import { LetterService } from 'src/app/services/letter.service';

import { AnimationService } from '../../../services/animation.service';

@Component({
  selector: 'app-guess',
  templateUrl: './guess.component.html',
  styleUrls: ['./guess.component.scss'],
})
export class GuessComponent implements OnInit {
  letter: string = '';
  @Input() index!: number;
  @Input() round: number = 0;
  @Input() currentSelection!: number;

  animationFinished: boolean = true;

  right: boolean = false;
  place: boolean = false;
  selected: boolean = false;
  flip: boolean = false;
  insert: boolean = false;
  shake: boolean = false;
  jump: boolean = false;

  constructor(
    private letterService: LetterService,
    private animationService: AnimationService
  ) {}

  ngOnInit(): void {}

  select() {
    if (this.index >= 5 * this.round && this.index < 5 * (this.round + 1))
      this.letterService.positionEvent.emit(this.index);
  }

  inRound(): boolean {
    return this.index >= this.round * 5 && this.index < (this.round + 1) * 5;
  }

  guessInsert() {
    this.insert = true;
    setTimeout(() => {
      this.insert = false;
    }, 100);
  }

  guessShake() {
    this.shake = true;
    setTimeout(() => {
      this.shake = false;
    }, 1000);
  }

  guessFlip() {
    this.flip = true;
  }

  guessJump() {
    this.jump = true;
  }

  setLetter(letter: string, animate: boolean = true) {
    this.letter = letter;
    if (animate) this.guessInsert();
  }

  setState(state: string) {
    if (state == 'right') {
      this.right = true;
    } else if (state == 'place') {
      this.place = true;
    }
  }
}
