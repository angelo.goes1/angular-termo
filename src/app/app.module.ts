import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GuessComponent } from './components/guesses/guess/guess.component';
import { GuessesComponent } from './components/guesses/guesses.component';
import { KeyComponent } from './components/keyboard/key/key.component';
import { KeyboardComponent } from './components/keyboard/keyboard.component';
import { ModalComponent } from './components/modal/modal.component';
import { KeyPipe } from './pipes/key.pipe';
import { NavbarComponent } from './components/navbar/navbar.component';
import { NotifyComponent } from './components/notify/notify.component';
import { ConfigsComponent } from './components/modal/configs/configs.component';
import { ToggleComponent } from './components/modal/configs/toggle/toggle.component';

@NgModule({
  declarations: [
    AppComponent,
    GuessesComponent,
    GuessComponent,
    KeyComponent,
    KeyboardComponent,
    ModalComponent,
    KeyPipe,
    NavbarComponent,
    NotifyComponent,
    ConfigsComponent,
    ToggleComponent,
  ],
  imports: [BrowserModule, AppRoutingModule],
  providers: [KeyPipe],
  bootstrap: [AppComponent],
})
export class AppModule {}
