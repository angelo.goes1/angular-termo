import { MenuService } from './services/menu.service';
import { Component } from '@angular/core';
import { LetterService } from './services/letter.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  openMenu: string = '';
  gameWon: string = 'hidden';

  constructor(
    private letterService: LetterService,
    private menuService: MenuService
  ) {
    this.letterService.winEvent.subscribe(() => {
      this.gameWon = 'visible';
    });
    this.letterService.restartEvent.subscribe(() => {
      this.gameWon = 'hidden';
    });
    this.menuService.openMenu.subscribe((menu) => {
      // if(this.openMenu == menu) this.openMenu = '';
      // else 
      this.openMenu = menu;
      console.log('iii');
      
    });
  }

  restart() {}

  closeMenu(event: MouseEvent) {
    console.log(event);
    
    this.menuService.openMenu.emit('');
  }
}
