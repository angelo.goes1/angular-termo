import { EventEmitter, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MenuService {
  openMenu: EventEmitter<string> = new EventEmitter();

  constructor() { }
}
