import { AnimationService } from 'src/app/services/animation.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-notify',
  templateUrl: './notify.component.html',
  styleUrls: ['./notify.component.scss'],
})
export class NotifyComponent implements OnInit {
  show: boolean = false;
  message: string = 'placeholder';

  constructor(private animationService: AnimationService) {
    this.animationService.notifyAnimation.subscribe((msg) => {
      this.message = msg ? msg : this.message;
      this.show = msg ? true : false;
    });
  }

  ngOnInit(): void {}
}
